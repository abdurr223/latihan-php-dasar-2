<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Function</title>
</head>

<body>
    <h1>Berlatih Function PHP</h1>
    <?php

    echo "<h3> Soal No 1 Greetings </h3>";
    /* 
        Soal No 1
        Greetings
        Buatlah sebuah function greetings() yang menerima satu parameter berupa string. 
        contoh: greetings("abduh");
        Output: "Halo Abduh, Selamat Datang di Sanbercode!"
    */

    // Code function di sini

    function greetings($nama)
    {
        echo "Halo " . $nama . ", Selamat Datang di Sanbercode! <br>";
    }

    greetings("Bagas");
    greetings("Wahyu");
    greetings("Abdurrahman");

    echo "<br>";
    echo "<h3>Soal No 2 Reverse String</h3>";
    /* 
            Soal No 2
            Reverse String
            Buatlah sebuah function reverseString() untuk mengubah string berikut menjadi kebalikannya menggunakan function dan looping (for/while/do while).
            Function reverseString menerima satu parameter berupa string.
            NB: DILARANG menggunakan built-in function PHP sepert strrev(), HANYA gunakan LOOPING!
            reverseString("abdul");
            Output: ludba
            
        */

    // Code function di sini 

    function reverseString($nama)
    {
        $panjang = strlen($nama);
        $reverseNama = "";

        for ($i = $panjang - 1; $i >= 0; $i--) {
            $reverseNama .= $nama[$i];
        }
        echo $reverseNama . "<br>";
    }
    reverseString("Abdurrahman");
    reverseString("Sanbercode");
    reverseString("We Are Sanbers Developer");
    echo "<br>";

    echo "<h3>Soal No 3 Palindrome </h3>";
    /*
    Soal No 3
    Palindrome
    Buatlah sebuah function yang menerima parameter berupa string yang mengecek apakah string tersebut sebuah palindrome atau bukan.
    Palindrome adalah sebuah kata atau kalimat yang jika dibalik akan memberikan kata yang sama contohnya: katak, civic.
    Jika string tersebut palindrome maka akan mengembalikan nilai true, sedangkan jika bukan palindrome akan mengembalikan false.
    NB:
    Contoh:
    palindrome("katak") => output : true
    palindrome("jambu") => output : false
    NB: DILARANG menggunakan built-in function PHP seperti strrev() dll. Gunakan looping seperti biasa atau gunakan function reverseString dari jawaban no.2!

    */


    // Code function di sini
    function palindrome($palindrome)
    {
        $panjang = strlen($palindrome);
        $reverseString = "";
        $hasilReverse = "";

        for ($i = $panjang - 1; $i >= 0; $i--) {
            $reverseString .= $palindrome[$i];
        }

        if ($palindrome == $reverseString) {
            $hasilReverse = "true";
        } else {
            $hasilReverse = "false";
        }

        echo $hasilReverse;
        echo "<br>";
    }


    // Hapus komentar di bawah ini untuk jalankan code
    palindrome("civic"); // true
    palindrome("nababan"); // true
    palindrome("jambaban"); // false
    palindrome("racecar"); // true

    echo "<h3>Soal No 4 Tentukan Nilai </h3>";

    function tentukan_nilai($nilai)
    {
        if ($nilai <= 100 && $nilai > 85) {
            $tertulis = "Sangat Baik";
        } else if ($nilai >= 75  && $nilai < 85) {
            $tertulis = "Baik";
        } else if ($nilai >= 60  && $nilai < 70) {
            $tertulis = "Cukup";
        } else {
            $tertulis = "Kurang";
        }
        return $tertulis;
    }
    // tentukan_nilai(90);
    echo tentukan_nilai(98) . "<br>";
    echo tentukan_nilai(76) . "<br>";
    echo tentukan_nilai(67) . "<br>";
    echo tentukan_nilai(43) . "<br>";
    ?>
</body>

</html>